import React, {Component} from 'react';
import { Provider } from 'react-redux';
import { store } from './redux/store/configStore';
import Routes from './Routes.js';
import { BrowserRouter as Router} from "react-router-dom";


export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Routes/>
        </Router>
      </Provider>
    )
  }
}
