import request from "./request";

export const getChannels = () => {
  return request
    .get("/channels")
    .then(res => {
      return res;
    })
    .catch(error => {
      return error;
    });
};

