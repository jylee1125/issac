import axios from "axios";

const host = "http://localhost:6553/api/v1";

const client = (token = null) => {
  const defaultOptions = {
    headers: {
      Accept: "*/*",
      "Content-Type": "application/json",
      Authorization: token ? `Bearer ${token}` : ""
    },
    baseURL: host
  };
  return {
    get: (url, options = {}) => axios.get(url, { ...defaultOptions, ...options }),
    post: (url, data, options = {}) => axios.post(url, data, { ...defaultOptions, ...options }),
    put: (url, data, options = {}) => axios.put(url, data, { ...defaultOptions, ...options })
  };
};

const request = client(localStorage.getItem("token"));

export default request;
