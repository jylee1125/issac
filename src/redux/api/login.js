import request from "./request";

export const login = userLogin => {
  return request
    .post("/auth/login", userLogin)
    .then(res => {
      return res;
    })
    .catch(error => {
      return error;
    });
};
